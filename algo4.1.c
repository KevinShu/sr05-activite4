/* Dans cette variante 1 de la quatrième solution, il n'y a pas de sémaphore.
 * Dans ce cas les deux threads pourraient être actifs simultanément sur une architecture multi-processeurs et le programme ne serait plus séquentiel.
 * Même sur un mono-processeur, le sémaphore reste nécessaire pour garantir l'atomicité des actions de lecture+traitement et de préparation+écriture.
 * En effet, s'il n'y a pas de sémaphore, l'ordonnanceur pourrait donner la main au second thread alors que le premier n'a pas terminé sa tâche.
 * Ce n'est pas une bonne solution. */

#include <stdio.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/wait.h>
#include <string.h>
#include <pthread.h>

// #define NB_THREAD 4
// #define LIMIT 2

void lire(int p) {
    char instring[20];
    read(p, instring, 7);
    instring[7] = '\0';

    printf("Message reçu: %s \n", instring);
    // attente pour vérification de l'atomicité
    for (int i=0; i<5; i++) {
        sleep(1);
        printf(". (bip lecteur)\n");
    }
}


int main() {

    int pid, pip[2];
    pipe(pip);
    pid = fork();
    // fils (nouveau thread)
    if (pid == 0){
        close(pip[1]);
        while(1){
            lire(pip[0]);
        }
    }
    // pere (thread original)
    else { 
        close(pip[0]);
        while(1){
            write(pip[1], "Bonjour", 7);
            printf("Message envoyé: Bonjour\n");
            // attente pour vérification de l'atomicité
            for (int i=0; i<5; i++) {
                sleep(1);
                printf(". (bip écrivain)\n");
            }
        }
    }
    return 0;
}