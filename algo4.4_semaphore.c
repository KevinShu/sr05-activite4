/* Dans cette variante 1 de la quatrième solution, il n'y a pas de sémaphore.
 * Dans ce cas les deux threads pourraient être actifs simultanément sur une architecture multi-processeurs et le programme ne serait plus séquentiel.
 * Même sur un mono-processeur, le sémaphore reste nécessaire pour garantir l'atomicité des actions de lecture+traitement et de préparation+écriture.
 * En effet, s'il n'y a pas de sémaphore, l'ordonnanceur pourrait donner la main au second thread alors que le premier n'a pas terminé sa tâche.
 * Ce n'est pas une bonne solution. */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>

#define NB_THREAD 2
// #define LIMIT 2

// création de sémaphore
sem_t semaphore;

void *lire(void *arg) {
    int tid = pthread_self();
    printf("Thread lecture [%i]\n", tid);
    while(1){
        char instring[20];
        read((int)arg, instring, 7);
        instring[7] = '\0';

        // attendre le sémaphore
        sem_wait(&semaphore);
        printf("Message reçu: %s \n", instring);
        // attente pour vérification de l'atomicité
        for (int i=0; i<5; i++) {
            sleep(1);
            printf(". (section critique lecteur)\n");
        }
        // rendre sémaphore
        sem_post(&semaphore);
        sched_yield();
    }
    pthread_exit(EXIT_SUCCESS);
}

void *ecrire(void *arg) {
    int tid = pthread_self();
    printf("Thread écriture [%i]\n", tid);
    while(1){
        // attendre le sémaphore
        sem_wait(&semaphore);
        write((int)arg, "Bonjour", 7);
        printf("Message envoyé: Bonjour\n");
        // attente pour vérification de l'atomicité
        for (int i=0; i<5; i++) {
            sleep(1);
            printf(". (section critique écrivain)\n");
        }
        // rendre le sémaphore
        sem_post(&semaphore);
        sched_yield();
    }
    pthread_exit(EXIT_SUCCESS);
}

int main() {
    // Création d'un tableau de thread
	pthread_t threads[NB_THREAD];

	// Initialisation du sémaphore
	sem_init(&semaphore, PTHREAD_PROCESS_SHARED, 1);

    int pip[2];
    pipe(pip);

    int err;
    if ((err = pthread_create(&threads[0], NULL, lire, (void*)pip[0])) != 0) {
			printf("Echec de la création du thread: [%s]", strerror(err));
			return EXIT_FAILURE;;
		}
    printf("Création du thread lecture\n");
    if ((err = pthread_create(&threads[1], NULL, ecrire, (void*)pip[1])) != 0) {
			printf("Echec de la création du thread: [%s]", strerror(err));
			return EXIT_FAILURE;;
		}
    printf("Création du thread écriture\n");
    

    for (int i = 0; i < NB_THREAD; i++) {
		pthread_join(threads[i], NULL);
	}

    sem_destroy(&semaphore);
    return 0;
}