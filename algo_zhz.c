#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

// longuer de string
#define BUFFER 100

// initializer mutex
pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;

void *lire(void *arg)
{
    // local variable pour que les autres threads ne pevent pas lire
    char msgRecu[BUFFER];

    while(1){
        // recevoir le message de stdin
        scanf("%s", msgRecu);

        // attendre le semaphore
        pthread_mutex_lock(&mtx);

        // afficher dans stderr
        fprintf(stderr, "<PID=%d> reception: %s\n", getpid(), msgRecu);
        fflush(stderr);

        // Empêcher ce thread d'utiliser trop de ressources de CPU
        sleep(0.2);

        // rendre le semaphore
        pthread_mutex_unlock(&mtx);
    }
}

int main()
{
    pthread_t mythread;
    // creer un thread
    if (pthread_create(&mythread, NULL, lire, NULL))
    {
        printf("error creating thread.");
        abort();
    }
    while(1){
        // attendre le semaphore
        pthread_mutex_lock(&mtx);

        printf("<PID=%d> dit: bonjour\n", getpid());
        fflush(stdout);

        // rendre le semaphore
        pthread_mutex_unlock(&mtx);

        // Empêcher ce thread d'utiliser trop de ressources de CPU
        sleep(1);
    }

    // jamais touche
    exit(0);
}