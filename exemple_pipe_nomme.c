#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>

#define PIPE "./fifo" 
int main() {
    int fd; char readbuf[20];
    mknod(PIPE, S_IFIFO | 0660, 0); // création du pipe fd= open(PIPE, O_RDONLY, 0); // ouvrir le pipe
    for (;;) {
        if (read(fd, &readbuf, sizeof(readbuf)) < 0){ // lire du pipe
            perror("Error reading pipe"); 
            exit(1); 
        }
        printf("Received string: %s\n", readbuf);
    } 
    exit(0); 
}