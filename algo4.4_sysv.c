/* Dans cette variante 1 de la quatrième solution, il n'y a pas de sémaphore.
 * Dans ce cas les deux threads pourraient être actifs simultanément sur une architecture multi-processeurs et le programme ne serait plus séquentiel.
 * Même sur un mono-processeur, le sémaphore reste nécessaire pour garantir l'atomicité des actions de lecture+traitement et de préparation+écriture.
 * En effet, s'il n'y a pas de sémaphore, l'ordonnanceur pourrait donner la main au second thread alors que le premier n'a pas terminé sa tâche.
 * Ce n'est pas une bonne solution. */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>


#define NB_THREAD 2 // thread ecrivain + thread lecteur
#define KEY 10086 // Unique semaphore key

void *lire(void *arg) {

    /* AFFICHAGE DE L'ID THREAD */
    int tid = pthread_self();
    printf("Thread lecture [%i]\n", tid);

    /* CONFIGURATION DU SEMAPHORE */
    struct sembuf operations[1];
    int retval;
    int id = semget(KEY, 1, 0666);
    if(id < 0){
      // Semaphore does not exist.
      fprintf(stderr, "Thread lire impossible de trouver le semaphore.\n");
      exit(0);
    }
    operations[0].sem_num = 0;
    operations[0].sem_op = -1;
    // comportement de semaphore, 0 pour default,  IPC_NOWAIT pour ne pas attendre
    operations[0].sem_flg = 0;

    /* BOUCLE DE LECTURE */
    while(1){
        char instring[20];
        read((int)arg, instring, 7);
        instring[7] = '\0';
        // attendre le sémaphore
        // P(S) décremente 1 de semaphore
        operations[0].sem_op = -1;
        // réaliser l'opération de décrémentation
        retval = semop(id, operations, 1);
        printf("// début section critique lecteur)\n");

        printf("Message reçu: %s \n", instring);
        // attente pour vérification de l'atomicité
        for (int i=0; i<5; i++) {
            sleep(1);
            printf(". (section critique lecteur)\n");
        }

        // rendre sémaphore
        printf("// fin section critique lecteur)\n");
        // V(S) incremente 1 de semaphore
        operations[0].sem_op = 1;
        // réaliser l'opération d'incrémentation
        retval = semop(id, operations, 1);

        // sched_yield();
    }
    pthread_exit(EXIT_SUCCESS);
}

void *ecrire(void *arg) {

    /* AFFICHAGE DE L'ID THREAD */
    int tid = pthread_self();
    printf("Thread écriture [%i]\n", tid);

    /* CONFIGURATION DU SEMAPHORE */
    struct sembuf operations[1];
    int retval;
    int id = semget(KEY, 1, 0666);
    if(id < 0){
      // Semaphore does not exist.
      fprintf(stderr, "Thread lire impossible de trouver le semaphore.\n");
      exit(0);
    }
    operations[0].sem_num = 0;
    operations[0].sem_op = -1;
    // comportement de semaphore, 0 pour default,  IPC_NOWAIT pour ne pas attendre 
    operations[0].sem_flg = 0;

    /* BOUCLE D'ECRITURE */
    while(1){

        // attendre le sémaphore
        // P(S) décremente 1 de semaphore
        operations[0].sem_op = -1;
        // réaliser l'opération de décrémentation
        retval = semop(id, operations, 1);
        printf("// début section critique écrivain)\n");

        write((int)arg, "Bonjour", 7);
        printf("Message envoyé: Bonjour\n");
        // attente pour vérification de l'atomicité
        for (int i=0; i<5; i++) {
            sleep(1);
            printf(". (section critique écrivain)\n");
        }

        // rendre le sémaphore
        printf("// fin section critique écrivain)\n");
        // V(S) incremente 1 de semaphore
        operations[0].sem_op = 1;
        // réaliser l'opération d'incrémentation
        retval = semop(id, operations, 1);

        // sched_yield();
    }
    pthread_exit(EXIT_SUCCESS);
}

int main() {
    // Création d'un tableau de thread
	pthread_t threads[NB_THREAD];

    // Identifiant de semaphore
    int id;

    // initialisation de l'argument supplémentaire utilisé dans semctl().
    // s'assure la valeur initiale de sémaphore sera 0
    union semun {
        int val;
        struct semid_ds *buf;
        ushort * array;
    } argument;
    
    argument.val = 1;
    // Donne key, nb semaphore et permission
    // dimension = 1 car un seul semaphore necessaire dans notre cas
    id = semget(KEY, 1, 0666 | IPC_CREAT);
  
    // Verification de la création et l'initialisation du sémaphore
    if(id < 0){
      fprintf(stderr, "Erreur de création du semaphore..\n");
      exit(0);
    }
    if( semctl(id, 0, SETVAL, argument) < 0){
      fprintf( stderr, "Erreur d'initialisation du semaphore.\n");
    } else {
      fprintf(stderr, "Semaphore %d initialisé.\n", KEY);
    }

    // Création de pipe de communication
    int pip[2];
    pipe(pip);

    // Lancement de deux thread
    int err;
    if ((err = pthread_create(&threads[0], NULL, lire, (void*)pip[0])) != 0) {
			printf("Echec de la création du thread: [%s]", strerror(err));
			return EXIT_FAILURE;;
		}
    printf("Création du thread lecture\n");
    if ((err = pthread_create(&threads[1], NULL, ecrire, (void*)pip[1])) != 0) {
			printf("Echec de la création du thread: [%s]", strerror(err));
			return EXIT_FAILURE;;
		}
    printf("Création du thread écriture\n");
    

    for (int i = 0; i < NB_THREAD; i++) {
		pthread_join(threads[i], NULL);
	}

    return 0;
}