# SR05 Activité4

| **Auteurs**             |   **Contact**              |
|:------------------------| :------------- |
| Bingqian Shu            |  bingqian.shu@etu.utc.fr |
| M. Bertrand Ducourthial |  bertrand.ducourthial@utc.fr |

## Réalisation
### Langage
Cette activité a été réalisé en utilisant le langage C.
### Alogorithme
Cette activité a été réalisé en utilisant l'algorithme suivant:
```agsl
Algorithme 4.4
fonction lire() {
    m1 <- lire(stdin) /* lecture bloquante */
    attendre le sémaphore
    traiter m1
    rendre sémaphore
}

créer le sémaphore

main() {
    créer un thread qui exécute lire()
    /* poursuite de l'exécution pour le thread principal */
    while(1)
        attendre le sémaphore
        préparer le message à écrire m2
        écrire(m2 stdout)
        rendre le sémaphore
}
```

## Différentes versions
### La version [algo.c](https://gitlab.com/KevinShu/sr05-activite4/-/blob/main/algo.c)

- Transfert de message par E/S standard de C
- Lecture directe après écriture, pas de concurrence

Pas bonne !

#### Résultat d'exécution
```agsl
Hello, World!
Hello, World!
Hello, World!
Hello, World!
Hello, World!
Hello, World!
Hello, World!
```

### La version [algo4.1.c](https://gitlab.com/KevinShu/sr05-activite4/-/blob/main/algo4.1.c)

- Transfert de message par pipe
- Lecture et écriture bloquantes
- Pas de sémaphore
- Programme pas séquentiel dans l'archi multi-processeur
- Pas de garantie d'atomicité

Pas bonne !

#### Résultat d'exécution
```agsl
Message envoyé: Bonjour
Message reçu: Bonjour 
. (bip écrivain)
. (bip lecteur)
. (bip écrivain)
. (bip lecteur)
. (bip écrivain)
. (bip lecteur)
. (bip écrivain)
. (bip lecteur)
. (bip écrivain)
Message envoyé: Bonjour
. (bip lecteur)
Message reçu: Bonjour 
. (bip écrivain)
. (bip lecteur)
. (bip écrivain)
. (bip lecteur)
. (bip écrivain)
. (bip lecteur)
. (bip écrivain)
```

### Fichier [exemple_pipe_nomme.c](https://gitlab.com/KevinShu/sr05-activite4/-/blob/main/exemple_pipe_nomme.c)

Exemple d'implémentation d'un pipe nommé

#### Résultat d'exécution
```agsl
Hello SR05
Received string: Hello SR05

Hello Sr05
Received string: Hello Sr05
```

### Fichier [exemple_semaphore.c](https://gitlab.com/KevinShu/sr05-activite4/-/blob/main/exemple_semaphore.c)

Exemple d'implémentation d'un sémaphore en C en utilisant la librairie `sémaphore.h`

#### Résultat d'exécution
```agsl
Création du thread numéro 0
Création du thread numéro 1
Je suis le thread [-323578304] et je vais dormir 1 seconde
Création du thread numéro 2
Création du thread numéro 3
Je suis le thread [-323578304] et j'ai fini ma sieste
Je suis le thread [-323578304] et je vais dormir 1 seconde
Je suis le thread [-323578304] et j'ai fini ma sieste
Je suis le thread [-340363712] et je vais dormir 1 seconde
Je suis le thread [-340363712] et j'ai fini ma sieste
Je suis le thread [-340363712] et je vais dormir 1 seconde
Je suis le thread [-340363712] et j'ai fini ma sieste
Je suis le thread [-348756416] et je vais dormir 1 seconde
Je suis le thread [-348756416] et j'ai fini ma sieste
Je suis le thread [-348756416] et je vais dormir 1 seconde
Je suis le thread [-348756416] et j'ai fini ma sieste
Je suis le thread [-331971008] et je vais dormir 1 seconde
Je suis le thread [-331971008] et j'ai fini ma sieste
Je suis le thread [-331971008] et je vais dormir 1 seconde
Je suis le thread [-331971008] et j'ai fini ma sieste


...Program finished with exit code 0
Press ENTER to exit console.
```

### Fichier [algo4.4_mutex.c](https://gitlab.com/KevinShu/sr05-activite4/-/blob/main/algo4.4_mutex.c)

Deuxième exemple d'implémentation d'un sémaphore en C en utilisant la librairie `sémaphore.h`

#### Résultat d'exécution
```agsl
Entered..

Just Exiting...

Entered..

Just Exiting...


...Program finished with exit code 0
Press ENTER to exit console.
```

### La version [algo4.4_semaphore.c](https://gitlab.com/KevinShu/sr05-activite4/-/blob/main/algo4.4_semaphore.c)

- Transfert de message par pipe
- Implémentation de sémaphore utilisant `sémaphore.h`
- Programme séquentiel
- Vérification de l'atomicité

L'algorithme est bonne.
Un problème a été constaté dans l'exécution : L'écrivain écrit beaucoup plus que le lecteur.
Le comportement devient normal quand on remplace les lignes d'implémentation de sémaphore avec celles dans la librairie `sys/types.h` `sys/ipc.h` et `sys/sem.h`, c.f. [algo4.4_sysv.c](https://gitlab.com/KevinShu/sr05-activite4/-/blob/main/algo4.4_sysv.c)

#### Résultat d'exécution
```agsl
Création du thread lecture
Création du thread écriture
Thread lecture [-1965640128]
Thread écriture [-1974032832]
Message envoyé: Bonjour
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
Message envoyé: Bonjour
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
Message reçu: Bonjour 
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
Message envoyé: Bonjour
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
Message reçu: Bonjour 
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
Message reçu: Bonjour 
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
Message envoyé: Bonjour
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
Message reçu: Bonjour 
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
Message envoyé: Bonjour
```

### La version [algo4.4_sysv.c](https://gitlab.com/KevinShu/sr05-activite4/-/blob/main/algo4.4_sysv.c)

Celle-ci est la version finale. Elle maintient les caractéritiques de [algo4.4_semaphore.c](https://gitlab.com/KevinShu/sr05-activite4/-/blob/main/algo4.4_semaphore.c) tout en assurant un accès à la section critique plus équilibre.

#### Résultat d'exécution
```agsl
Semaphore 10086 initialisé.
Création du thread lecture
Création du thread écriture
Thread écriture [-1606822336]
// début section critique écrivain)
Message envoyé: Bonjour
Thread lecture [-1598429632]
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
// fin section critique écrivain)
// début section critique lecteur)
Message reçu: Bonjour 
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
// fin section critique lecteur)
// début section critique écrivain)
Message envoyé: Bonjour
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
// fin section critique écrivain)
// début section critique lecteur)
Message reçu: Bonjour 
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
// fin section critique lecteur)
// début section critique écrivain)
Message envoyé: Bonjour
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
. (section critique écrivain)
// fin section critique écrivain)
// début section critique lecteur)
Message reçu: Bonjour 
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
. (section critique lecteur)
// fin section critique lecteur)
```