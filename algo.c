#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#define MAX_MSG_LEN 256

int main(int argc, char* argv[]) {
    char* message = "Hello, World!"; // message par défaut
    char input[MAX_MSG_LEN];
    int fd_stdin = fileno(stdin);
    int flags = fcntl(fd_stdin, F_GETFL, 0);
    fcntl(fd_stdin, F_SETFL, flags | O_NONBLOCK); // mettre l'entrée standard en mode non-bloquant
    while (1) {
        printf("%s\n", message); // émission périodique du message
        fflush(stdout);
        ssize_t n = read(fd_stdin, input, MAX_MSG_LEN); // lecture asynchrone de l'entrée standard
        if (n > 0) {
            input[n] = '\0';
            fprintf(stderr, "réception de %s", input); // affichage sur la sortie erreur standard
        }
        sleep(1); // attente de 1 seconde
    }
    return 0;
}

/*
Le programme commence par définir une chaîne de caractères message qui représente le message à émettre périodiquement. 
Cette chaîne peut être modifiée à l'exécution du programme si une interface graphique est implémentée.
Ensuite, le programme définit un tampon input de taille MAX_MSG_LEN pour stocker les données reçues sur l'entrée standard.
Le programme utilise la fonction fileno pour récupérer le descripteur de fichier de l'entrée standard (stdin) et la fonction fcntl pour mettre ce descripteur en mode non-bloquant (O_NONBLOCK). 
Cette configuration permet au programme de lire de manière asynchrone l'entrée standard en utilisant la fonction read.
La boucle principale du programme contient deux parties :
La première partie émet périodiquement le message en utilisant la fonction printf suivie de fflush(stdout) pour s'assurer que le message est immédiatement écrit sur la sortie standard sans être mis en mémoire tampon.
La deuxième partie utilise la fonction read pour lire les données de l'entrée standard de manière asynchrone. 
Si des données sont disponibles, le programme affiche le message "réception de xxx" sur la sortie erreur standard (stderr) en utilisant la fonction fprintf.
Le programme attend 1 seconde entre chaque itération de la boucle principale en utilisant la fonction sleep.
*/